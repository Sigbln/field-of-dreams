# Клиент-сервер "Поле чудес"
## Установка
* Склонируйте репозиторий
``` 
https://gitlab.com/Sigbln/field-of-dreams.git
cd field-of-dreams/
 ```
* В разных окнах терминала выполнить эти команды
```
make run_server
```

![](/images/run_server.png)
```
make run_client
```
![](/images/run_client.png)
* Наслаждайтесь игрой)
* Чтобы завершить сервер выполните
```
make drop_server
```


## Пару скриншотов процесса
![Победа](/images/victory.png)
![Поражение](/images/lost.png)
